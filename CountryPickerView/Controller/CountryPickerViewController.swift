//
//  CountryPickerVC.swift
//  CountryPickerView
//
//  Created by webcastle on 23/05/22.
//

import UIKit
import CountryPicker

class CountryPickerViewController: UIViewController {

    @IBOutlet weak var txtCountry: UITextField!
    
    let picker = CountryPickerVC()
    
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        picker.delegate = self
        
    }
    

    @IBAction func tappedTextField(_ sender: UIButton) {
        picker.openCountriesList(from: self)
    }
    
    

}

extension CountryPickerViewController: CountryPickerViewDelegate {
    
    func countryPickerView(_ countryPickerView: CountryPickerVC, didSelectcountry country: String) {
        self.txtCountry.text = country
    }
    
}
